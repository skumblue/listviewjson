package com.example.diego.listviewjson;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.InputStream;


public class MainActivity extends ActionBarActivity {

	ListView lstOpciones;
	Titular[] datos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Construct the data source
		/*ArrayList<User> arrayOfUsers = new ArrayList<User>();

		arrayOfUsers.add(new User("1","11"));
		arrayOfUsers.add(new User("2","22"));
		arrayOfUsers.add(new User("3","33"));
		arrayOfUsers.add(new User("4","44"));
		arrayOfUsers.add(new User("5","55"));
		arrayOfUsers.add(new User("6","66"));
		arrayOfUsers.add(new User("7","77"));
		arrayOfUsers.add(new User("8","88"));*/

		  datos =
				new Titular[]{
						new Titular("Título 0", "http://www.es.clipproject.info/images/joomgallery/details/diversos_dibujos_41/uno_clipart_20110418_1238305358.gif"),
						new Titular("Título 1", "http://2.bp.blogspot.com/-ZiznOI1QGzQ/TbxlsbMqn3I/AAAAAAAATX8/azQXET7wGok/s400/dos.gif"),
						new Titular("Título 2", "http://2.bp.blogspot.com/-DrO05oB45wM/U5eMOGI6olI/AAAAAAAAFYE/aXzq98fFOmw/s1600/tres-300x300.png"),
						new Titular("Título 3", "http://www.curriculumenlineamineduc.cl/605/articles-24137_thumbnail.jpg"),
						new Titular("Título 4", "http://www.aguam.com/wp-content/uploads/2010/07/_1216331449805.jpg")};

		AdaptadorTitulares adaptador =
				new AdaptadorTitulares(this);

		lstOpciones = (ListView)findViewById(R.id.listView_cupones);

		lstOpciones.setAdapter(adaptador);
	}

	public class Titular
	{
		private String titulo;
		private String subtitulo;

		public Titular(String tit, String sub){
			titulo = tit;
			subtitulo = sub;

		}

		public String getTitulo(){
			return titulo;
		}

		public String getSubtitulo(){
			return subtitulo;
		}
	}

	class AdaptadorTitulares extends ArrayAdapter {

		Activity context;

		AdaptadorTitulares(Activity context) {
			super(context, R.layout.view_coupon, datos);
			this.context = context;
		}

		public View getView(int position, View convertView, ViewGroup parent)
		{
			View item = convertView;
			ViewHolder holder;


			if(item == null)
			{
				LayoutInflater inflater = context.getLayoutInflater();
				item = inflater.inflate(R.layout.view_coupon, null);

				holder = new ViewHolder();
				holder.titulo = (TextView)item.findViewById(R.id.titulo_coupon);
				holder.subtitulo = (TextView)item.findViewById(R.id.generated_coupon);
				holder.image_coupon = (ImageView)item.findViewById(R.id.image_coupon);

				item.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)item.getTag();
			}

			String url = datos[position].getSubtitulo();
			Log.i("POS", String.valueOf(position)+", Item: "+datos[position].getTitulo());
			new DownloadImageTask(holder.image_coupon, position).execute(url);
			//Log.i("POS", String.valueOf(position));

			holder.titulo.setText(datos[position].getTitulo());
			holder.subtitulo.setText(datos[position].getSubtitulo());

			return(item);
		}
	}



	//clase que guardará la referencia de cada cupon de la lista (para no buscarlos por id en cada iteracion de getView)
	static class ViewHolder {
		TextView titulo;
		TextView subtitulo;
		ImageView image_coupon;
	}


	//get image async
	//String: parameter doInBackground
	//Void: parameter onProgressUpdate
	//Bitmap: return doInBackground and parameter onPostExecute
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		private int mPosition;

		public DownloadImageTask(ImageView bmImage, int position) {
			this.bmImage = bmImage;
			this.mPosition = position;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0]; //param url image
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		/*
		@Override
		protected void onProgressUpdate(Void... values){
			//mostrar loader
		}
		*/

		protected void onPostExecute(Bitmap result) {

			bmImage.setImageBitmap(result);

		}
	}
	//end get image async

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
